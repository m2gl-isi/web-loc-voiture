/**
 * View Models used by Spring MVC REST controllers.
 */
package sn.gaaynako.locationvoiture.web.rest.vm;
